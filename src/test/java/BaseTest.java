import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import ua.rozborskyi.pages.BasePage;
import ua.rozborskyi.pages.ResultPage;
import ua.rozborskyi.utilits.DriverManager;

public class BaseTest {
    private static final Logger logger = LoggerFactory.getLogger(ResultPage.class);

    @BeforeMethod
    public void startTest(){
        BasePage.setDriver(DriverManager.getDriver());
    }

    @AfterMethod
    public void endTest(){
        logger.info("System close driver");
        DriverManager.closeDriver();
    }
}