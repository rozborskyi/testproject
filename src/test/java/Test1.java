import org.testng.Assert;
import org.testng.annotations.Test;
import ua.rozborskyi.pages.GooglePage;
import ua.rozborskyi.pages.ResultPage;
import ua.rozborskyi.pages.SearchResultPage;

public class Test1 extends BaseTest{

    @Test(description = "Open Google. Search for 'automation'. Open the first link on search results page. Verify that title contains searched word")
    public void searchWordAutomation() {
        String wordForSearch = "automation";

        GooglePage googlePage = new GooglePage();
        googlePage
                .openStartPage()
                .search(wordForSearch);
        new SearchResultPage()
                .openFirstLink();
        String title = new ResultPage().getTitle();

        Assert.assertTrue(title.toLowerCase().contains(wordForSearch),
                "Word '" + wordForSearch + "' isn't present in the title of first opened link\t");
    }
}