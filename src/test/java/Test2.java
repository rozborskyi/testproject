import org.testng.Assert;
import org.testng.annotations.Test;
import ua.rozborskyi.pages.GooglePage;
import ua.rozborskyi.pages.ResultPage;
import ua.rozborskyi.pages.SearchResultPage;

public class Test2  extends BaseTest{

    @Test(description = "Open Google. Search for 'automation'. Verify that there is expected domain ('testautomationday.com') on search results  pages (page: 1-5)")
    public void searchTestautomationday() {
        String wordForSearch =  "automation";
        String domainName =     "testautomationday.com";

        GooglePage googlePage = new GooglePage();
        googlePage
                .openStartPage()
                .search(wordForSearch);

        boolean isDomainPresent = false;
        for (int i = 0; i < 5; i++) {
            isDomainPresent = new SearchResultPage().isDomainPresent(domainName);
            if (isDomainPresent) {
                break;
            }
            new SearchResultPage()
                    .goToNextPage();
        }

        Assert.assertTrue(isDomainPresent, "Domain '" + domainName + "' isn't present on the first 5 pages with search results\t");
    }
}