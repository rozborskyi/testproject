package ua.rozborskyi.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.rozborskyi.utilits.DriverManager;
import ua.rozborskyi.utilits.PropertiesManager;

public class BasePage {
    private static final Logger logger = LoggerFactory.getLogger(DriverManager.class);
    private String baseUrl = PropertiesManager.url;
    private int TIME_FOR_ELEMENT_WAITING = 10;
    protected static WebDriver driver;

    /**
     * set driver
     * @param webDriver            instance of Webdriver
     */
    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }

    /**
     * run browser and open page 'https://www.google.com'
     */
    public void open() {
        logger.info("Opening page: " + baseUrl);
        driver.get(baseUrl);
    }

    /**
     * tries to find element
     * @param by        By class
     */
    public WebElement findElement(By by) {
        try {
            (new WebDriverWait(driver, TIME_FOR_ELEMENT_WAITING))
                    .until(ExpectedConditions.visibilityOfElementLocated(by));
            return driver.findElement(by);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RuntimeException("Failure finding element");
        }
    }

    /**
     * get title of the page
     * @return              title of the page
     */
    public String getTitle() {
        return driver.getTitle();
    }
}