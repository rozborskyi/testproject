package ua.rozborskyi.pages;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page with result of search that is displayed to the user
 */
public class SearchResultPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(SearchResultPage.class);
    private static SearchResultPage instance = createInstance();

    private static SearchResultPage createInstance() {
        return instance == null
                ? new SearchResultPage()
                : instance;
    }

    /**
     * opens first link on the result page
     * @return              instance of GooglePage
     */
    public SearchResultPage openFirstLink() {
        logger.info("Opens first link on the result page");

        By searchBar = By.xpath("//a/h3");
        findElement(searchBar)
                .click();

        return SearchResultPage.instance;
    }

    /**
     * checks is domain present on the page
     * @param domain        domain
     * @return              is domain present on the page
     */
    public boolean isDomainPresent(String domain) {
        boolean isDomainPresent = true;
        By domainBy = By.xpath("//cite[contains(text(), '" + domain + "')]");

        try {
            findElement(domainBy);
        } catch (RuntimeException e) {
            isDomainPresent = false;
        }

        return isDomainPresent;
    }

    /**
     * clicks on arrow, which redirect to the next page with result
     * @return              is domain present on the page
     */
    public SearchResultPage goToNextPage() {
        logger.info("Clicks on arrow, which redirects to the next page");
        By nextPageBy = By.id("pnnext");
        findElement(nextPageBy)
                .click();

        return SearchResultPage.instance;
    }
}
