package ua.rozborskyi.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Start google page
 */
public class GooglePage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(GooglePage.class);
    private static GooglePage instance = createInstance();

    private static GooglePage createInstance() {
        return instance == null
                ? new GooglePage()
                : instance;
    }

    /**
     * open page 'https://www.google.com'
     * @return          instance of GooglePage
     */
    public GooglePage openStartPage() {
        open();

        return GooglePage.instance;
    }

    /**
     * insert text to search bar
     * @param text          text
     * @return              instance of GooglePage
     */
    public GooglePage search(String text) {
        inserTextToSearch(text);
        clickEnterButton();

        return GooglePage.instance;
    }

    /**
     * insert text to search bar
     * @param text          text
     * @return              instance of GooglePage
     */
    public GooglePage inserTextToSearch(String text) {
        logger.info("Insert '" + text + "' to search bar");

        By searchBar = By.xpath("//form//input[@type='text']");
        findElement(searchBar)
                .sendKeys(text);

        return GooglePage.instance;
    }

    /**
     * clicks on button Enter
     * @return              instance of GooglePage
     */
    public GooglePage clickEnterButton() {
        logger.info("Clicks on button Enter");

        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ENTER)
                .click()
                .build()
                .perform();

        return GooglePage.instance;
    }
}