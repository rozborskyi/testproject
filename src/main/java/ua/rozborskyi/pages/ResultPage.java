package ua.rozborskyi.pages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page that is displayed to the user after clicking on the link on the page with search results
 */
public class ResultPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(ResultPage.class);
    private static ResultPage instance = createInstance();

    private static ResultPage createInstance() {
        return instance == null
                ? new ResultPage()
                : instance;
    }
}