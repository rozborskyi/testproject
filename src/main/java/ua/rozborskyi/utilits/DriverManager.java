package ua.rozborskyi.utilits;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriverManager {
    private DriverManager(){}
    private static final Logger logger = LoggerFactory.getLogger(DriverManager.class);
    private static WebDriver driver = createDriverInstance();

    /**
     * returns instance of WebDriver
     * @return          WebDriver
     */
    public static WebDriver getDriver() {
        return driver;
    }

    /**
     * creates instance of WebDriver
     * @return          WebDriver
     */
    private static WebDriver createDriverInstance() {
        logger.info("System creates driver");
        String browserName = PropertiesManager.browserName;

        return browserName.equals("chrome")
                ? createChromeDriverInstance()
                : createFirefoxDriverInstance();
    }

    /**
     * creates instance of ChromeDriver
     * and set options
     * @return          ChromeDriver
     */
    private static WebDriver createChromeDriverInstance() {
        logger.info("System creates ChromeDriver");

        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--no-cache");

        return new ChromeDriver(options);
    }

    /**
     * method only for example
     */
    private static WebDriver createFirefoxDriverInstance() {
        return new ChromeDriver();
    }

    /**
     * destroy driver and close browser
     */
    public static void closeDriver() {
        driver.quit();
    }
}