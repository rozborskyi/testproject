package ua.rozborskyi.utilits;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {
    public static String browserName;
    public static String url;

    static{
        try (InputStream input = new FileInputStream("src/main/resources/testProject.properties")) {
            Properties prop = new Properties();
            prop.load(input);

            browserName = prop.getProperty("browser");
            url = prop.getProperty("url");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}